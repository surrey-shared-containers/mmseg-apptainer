BootStrap: docker
From: continuumio/miniconda3:4.9.2

%post
    apt-get update && apt-get install -y \
        libgl1-mesa-glx \
        libglib2.0-0 \
        libxrender1 \
        libxext6 \
        libsm6 \
        intel-oneapi-runtime-openmp intel-oneapi-runtime-itt

    # Clean up apt-get cache to free space
    apt-get clean
    #rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

    # Install Conda and packages
    . /opt/conda/etc/profile.d/conda.sh
    conda activate base
    conda install -c conda-forge python=3.9 mamba

    # Install PyTorch and other packages
    mamba install mkl==2024.0 pytorch==1.10.1 torchvision==0.11.2 torchaudio==0.10.1 cudatoolkit=11.3 -c pytorch -c conda-forge

    # Clean up conda/mamba cache and unnecessary packages
    mamba clean --all -y
    rm -rf /opt/conda/pkgs/*

    # Install pip packages
    pip install --no-cache-dir timm==0.4.12 scipy==1.10.1 yapf==0.40.0 fvcore openmim ftfy regex
    #pip install --no-cache-dir mmcv-full==1.4.4 -f https://download.openmmlab.com/mmcv/dist/cu113/torch1.10.0/index.html
    mim install mmcv-full==1.4.4
    pip install --no-cache-dir mmsegmentation==0.24.0
    #mim install mmsegmentation==0.24.0
    pip install --no-cache-dir --upgrade opencv-python-headless
    pip install einops
    pip install --no-deps faiss-gpu

    # Clean up pip cache
    rm -rf ~/.cache/pip

    # Clean up unnecessary files from conda, pip, and system
    find /opt/conda/ -type f -name '*.pyc' -delete
    find /opt/conda/ -type f -name '*.pyo' -delete
    find /opt/conda/ -type d -name '__pycache__' -exec rm -rf {} +
    #rm -rf /tmp/* /var/tmp/*

%runscript
    # Activate the conda environment at runtime
    . /opt/conda/etc/profile.d/conda.sh
    conda activate base