apptainer-build-ci
Project containing a shared gitlab-ci.yml file for building Apptainer images.

How to use
Simply create a .gitlab-ci.yml file in your project that contains the following:

include:
  - project: 'surrey-shared-containers/templates/apptainer-build-ci'
    file: 'apptainer-build-ci.yml'
This will cause you project to pull and use the gitlab-ci.yml from this project.